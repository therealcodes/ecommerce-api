<?php

namespace Database\Seeders;

use App\Enums\RoleType;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermissionsDemoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // create permissions
        Permission::create(['name' => 'edit articles']);
        Permission::create(['name' => 'delete articles']);
        Permission::create(['name' => 'publish articles']);
        Permission::create(['name' => 'unpublish articles']);

        // create roles and assign existing permissions
        $role1 = Role::create(['name' => RoleType::fromValue(RoleType::CUSTOMER)->description]);
        // $role1->givePermissionTo('edit articles');
        // $role1->givePermissionTo('delete articles');


        $role2 = Role::create(['name' => RoleType::fromValue(RoleType::SELLER)->description]);
        $role2->givePermissionTo('publish articles');
        $role2->givePermissionTo('unpublish articles');

        $role3 = Role::create(['name' => RoleType::fromValue(RoleType::OWNER)->description]);
        // gets all permissions via Gate::before rule; see AuthServiceProvider

        // create demo users
        $user = User::create([
            'first_name' => 'Amit',
            'last_name' => 'Verma',
            'email' => 'customer@gmail.com',
            'password' => Hash::make('Amit@1001'),

        ]);
        $user->assignRole($role1);

        $user = User::create([
            'first_name' => 'Example',
            'last_name' => 'Admin User',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('Admin@1001'),

        ]);
        $user->assignRole($role2);

        $user = User::create([
            'first_name' => 'Example',
            'last_name' => 'Super-Admin User',
            'email' => 'superadmin@gmail.com',
            'password' => Hash::make('Superadmin@1001'),

        ]);
        $user->assignRole($role3);
    }
}
