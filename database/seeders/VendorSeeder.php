<?php

namespace Database\Seeders;

use App\Enums\FirmType;
use App\Models\Vendor;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class VendorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = Vendor::create([
            'user_id' => 2,
            'firm_name' => 'Aditi Agro Foods',
            'firm_type' => FirmType::Manufacture,
            'address' => '',
            'country' => 'India',
            'state' => 'Uttar Pradesh',
            'city' => 'Lakhimpur Kheri',
            'post_code' => '262701',
            'bank_name' => 'Kotak Mahindra Bank',
            'bank_address' => 'Kotak Mahindra Bank Lucknow - Ashiyana Branch Address',
            'beneficiary_name' => 'Amit Verma',
            'ifsc_code' => 'KKBK0005199 ',
            'bank_account_no' => '18602662666',
            'is_activated' => Vendor::ACTIVATED,
            'created_by' => '1',
        ]);
    }
}
