<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendors', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');;
            $table->string('firm_name');
            $table->integer('firm_type');
            $table->string('address');
            $table->string('country');
            $table->string('state');
            $table->string('city');
            $table->string('post_code');
            $table->string('logo', 32)->nullable();
            $table->string('bank_name')->default(null)->nullable();
            $table->string('bank_address')->default(null)->nullable();
            $table->string('beneficiary_name')->default(null)->nullable();
            $table->string('ifsc_code')->default(null)->nullable();
            $table->string('bank_account_no')->default(null)->nullable();
            $table->tinyInteger('is_activated')->default(0);
            $table->softDeletes();
            $table->integer('created_by')->unsigned()->default(0);
            $table->integer('updated_by')->unsigned()->default(0);
            $table->integer('deleted_by')->unsigned()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendors');
    }
};
