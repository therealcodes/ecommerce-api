@component('mail::message')
# Hello

Please enter the code:

@component('mail::button', ['url' => ''])
    {{ $verification_code }}
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
