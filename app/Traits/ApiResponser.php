<?php

namespace App\Traits;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\App;

trait ApiResponser
{
    /**
     * @param array|object $data
     * @param string $message
     * @param int $status_code
     * @return JsonResponse
     */
    public function responseSuccess($data, string $message = "Successful", int $status_code): JsonResponse
    {
        return $this->_response(true, $data, $message, [], $status_code);
    }

    /**
     * @param array $errors
     * @param string $message
     * @param int $status_code
     * @return JsonResponse
     */
    public function responseError(array $errors, string $message = 'Something went wrong.', int $status_code): JsonResponse
    {
        return $this->_response(false, [], $message, $errors, $status_code);
    }

    /**
     * @param bool $status
     * @param array|object $data
     * @param string $message
     * @param array $errors
     * @param int $status_code
     * @return JsonResponse
     */
    private function _response(bool $status, $data, string $message, array $errors, int $status_code): JsonResponse
    {
        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $data,
            'errors' => (!App::environment('production')) ? $errors : [],
        ], $status_code);
    }
}
