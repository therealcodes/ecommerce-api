<?php

namespace App\Http\Controllers\vendor;

use App\Models\User;
use App\Models\Vendor;
use App\Traits\ApiResponser;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Requests\VendorRegistrationRequest;

class VendorController extends Controller
{
    use ApiResponser;

    public function register(VendorRegistrationRequest $request, $user_id)
    {
        try {
            $payload = $request->post();
            $payload['user_id'] = $user_id;

            $seller = Vendor::whereUserId($user_id)->first();

            if (empty($seller)) {
                $seller = Vendor::create($payload);
                $message = "Seller created successfully.";
            } else {
                Vendor::whereUserId($user_id)->update($payload);
                $seller->refresh();
                $message = "Seller updated successfully.";
            }

            $data = [
                'seller' => $seller,
                'user' => User::find($user_id)
            ];
            return $this->responseSuccess($data, $message, Response::HTTP_OK);
        } catch (\Exception $e) {
            return $this->responseError([$e->getMessage()], '', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
