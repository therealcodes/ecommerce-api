<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Notifications\AppEmailVerificationNotification;
use App\Traits\ApiResponser;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;

class PasswordResetController extends Controller
{
    use ApiResponser;

    public function forgetRequest(Request $request)
    {
        if ($request->send_code_by == 'email') {
            $user = User::where('email', $request->email_or_phone)->first();
        } else {
            $user = User::where('phone', $request->email_or_phone)->first();
        }

        if (!$user) {
            return $this->responseSuccess([], "User is not found.", Response::HTTP_NOT_FOUND);
        } else {

            $user->verification_code = rand(100000, 999999);
            $user->save();
            if ($request->send_code_by == 'phone') {
                return  true;

                // $otpController = new OTPVerificationController();
                // $otpController->send_code($user);
            } else {
                $user->notify(new AppEmailVerificationNotification());
            }
        }

        return $this->responseSuccess([], "A code is sent.", Response::HTTP_OK);
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function confirmReset(Request $request): JsonResponse
    {
        $user = User::where('verification_code', $request->verification_code)->first();

        if ($user != null) {
            $user->verification_code = null;
            $user->password = $request->password;
            $user->save();

            return $this->responseSuccess([], "Your password is reset.Please login.", Response::HTTP_OK);
        } else {
            return $this->responseSuccess([], "No user is found", Response::HTTP_OK);
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function resendCode(Request $request): JsonResponse
    {

        if (request('send_code_by') == 'email') {
            $user = User::where('email', request('email_or_phone'))->first();
        } else {
            $user = User::where('phone', request('email_or_phone'))->first();
        }


        if (!$user) {
            return $this->responseSuccess([], "User is not found.", Response::HTTP_NOT_FOUND);
        }


        $user->verification_code = rand(100000, 999999);
        $user->save();

        if ($request->send_code_by == 'email') {
            $user->notify(new AppEmailVerificationNotification());
        } else {
            // $otpController = new OTPVerificationController();
            // $otpController->send_code($user);
        }
        return $this->responseSuccess([], "A code is sent again", Response::HTTP_OK);
    }
}
