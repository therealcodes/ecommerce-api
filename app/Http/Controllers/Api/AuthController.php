<?php

namespace App\Http\Controllers\Api;

use App\Enums\RoleType;
use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\StoreUserRequest;
use App\Http\Resources\RoleCollection;
use App\Services\RoleService;
use App\Services\UserService;
use App\Traits\ApiResponser;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends Controller
{
    use ApiResponser;

    /**
     * The user service instance.
     */
    private UserService $userService;
    private RoleService $roleService;

    /**
     * Create a new controller instance.
     *
     * @param UserService $userService
     * @param RoleService $roleService
     */
    public function __construct(UserService $userService, RoleService $roleService)
    {
        $this->userService = $userService;
        $this->roleService = $roleService;
    }


    /**
     * API auth
     *
     * @param LoginRequest $request
     * @return JsonResponse
     */
    public function login(LoginRequest $request): JsonResponse
    {
        try {
            if (auth()->attempt(['email' => $request->email, 'password' => $request->password])) {
                $user = auth()->user();
                $data = $this->loginSuccess($user);
                return $this->responseSuccess($data, "Successfully logged in.", Response::HTTP_OK);
            }

            return $this->responseError([], "Email or password is invalid.", Response::HTTP_UNPROCESSABLE_ENTITY);
        } catch (\Exception $e) {
            return $this->responseError([$e->getMessage()], '', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Handle a registration request for the application.
     *
     * @param StoreUserRequest $request
     * @return JsonResponse
     */
    public function register(StoreUserRequest $request): JsonResponse
    {
        $roleType = RoleType::CUSTOMER;
        return $this->registerUser($request, $roleType);
    }

    /**
     * Handle a seller registration request for the application.
     *
     * @param StoreUserRequest $request
     * @return JsonResponse
     */
    public function sellerSignup(StoreUserRequest $request): JsonResponse
    {
        $roleType = RoleType::SELLER;
        return $this->registerUser($request, $roleType);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function user(Request $request): JsonResponse
    {
        return response()->json($request->user());
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function logout(Request $request): JsonResponse
    {
        $request->user()->tokens()->delete();

        return $this->responseSuccess([], "Successfully logged out.", Response::HTTP_OK);
    }

    /**
     * @param $user
     * @return array
     */
    protected function loginSuccess($user): array
    {
        $token = $user->createToken('authToken')->accessToken;

        return [
            'user' => [
                'id' => $user->id,
                'name' => $user->full_name,
                'email' => $user->email,
                'avatar' => $user->avatar,
                'avatar_original' => '',
                'phone' => $user->phone
            ],
            'roles' => new RoleCollection($user->roles),
            'access_token' => $token,
            'token_type' => 'Bearer',
            'expires_at' => null
        ];
    }

    protected function registerUser($request, $roleType)
    {
        try {
            $payload = $request->all();
            $user = $this->userService->register($payload);

            if (!empty($user)) {
                $this->roleService->assignRole($user, $roleType);
                $data = $this->loginSuccess($user);
                return $this->responseSuccess($data, 'Registered successfully.', Response::HTTP_OK);
            } else {
                return $this->responseError([], "Registration not completed. Please try again.", Response::HTTP_UNPROCESSABLE_ENTITY);
            }
        } catch (\Exception $e) {
            return $this->responseError([$e->getMessage()], '', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
