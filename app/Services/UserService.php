<?php

namespace App\Services;

use App\Repository\UserRepositoryInterface;

class UserService
{
    private $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function register(array $payload)
    {
        return $this->userRepository->create($payload);
    }
}
