<?php

namespace App\Services;

use App\Repository\RoleRepositoryInterface;

class RoleService
{
    private $roleRepository;

    public function __construct(RoleRepositoryInterface $role)
    {
        $this->role = $role;
    }

    public function assignRole($user, $role)
    {
        return $user->assignRole($role);
    }
}
