<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class RoleType extends Enum
{
    const CUSTOMER =  1;
    const SELLER = 2;
    const OWNER = 3;
    const GUEST = 4;
}
