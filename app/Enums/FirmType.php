<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static Retailer()
 * @method static static Manufacture()
 * @method static static Wholesaller()
 * @method static static Distribution()
 */
final class FirmType extends Enum
{
    const Retailer =   1;
    const Manufacture =   2;
    const Wholesaller = 3;
    const Distribution = 4;
}
