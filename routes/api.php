<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\PasswordResetController;
use App\Http\Controllers\vendor\VendorController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Auth Route
|--------------------------------------------------------------------------
*/

Route::group(['prefix' => 'auth', 'middleware' => ['json.response']], function () {
    Route::post('/login', [AuthController::class, 'login']);
    Route::post('/signup', [AuthController::class, 'register']);

    Route::post('password/forget-request', [PasswordResetController::class, 'forgetRequest']);
    Route::post('password/confirm_reset', [PasswordResetController::class, 'confirmReset']);
    Route::post('password/resend_code', [PasswordResetController::class, 'resendCode']);


    Route::group(['prefix' => 'vendor'], function () {
        Route::patch('signup', [AuthController::class, 'sellerSignup']); // seller signup
    });

    Route::middleware('auth:api')->group(function () {
        Route::get('logout', [AuthController::class, 'logout']);
        Route::get('user', [AuthController::class, 'user']);

        Route::group(['prefix' => 'vendor'], function () {
            Route::post('registration/{user_id}', [VendorController::class, 'register']); // seller registration
        });
    });
});
